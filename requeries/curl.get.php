<?php 

// GET API

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://embedsocial.com/admin/v2/api/reviews?reviews_ref=0d44e0b0a245de6fc9651f870d8b44efc4653184',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER => [
        "Accept: application/json",
        "Content-Type: application/json; charset=utf-8",
        "Authorization: Bearer escfe7569d859dd903d77664e9983edf",
    ],
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
));
$response = curl_exec($curl);
curl_close($curl);
$data = json_decode($response, true);
$reviews = $data['reviews'];
