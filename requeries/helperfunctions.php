<?php

// HELPER FUNCTIONS

function sortByRatingAndDate($a, $b)
{
    if ($a['rating'] === $b['rating']) {
        if ($a['reviewCreatedOnDate'] === $b['reviewCreatedOnDate']) {
            return $a <=> $b;
        }
        if ($_POST['sortByDate'] === 'oldest') {
            return $a['reviewCreatedOnDate'] <=> $b['reviewCreatedOnDate'];
        }
        return $b['reviewCreatedOnDate'] <=> $a['reviewCreatedOnDate'];
    }

    if ($_POST['sortByRating'] === 'lowest') {
        return $a['rating'] <=> $b['rating'];
    }
    return $b['rating'] <=> $a['rating'];
}

function filterByMinimumRating($review)
{
    return $review['rating'] >= $_POST['filterByRating'];
}

function filterWithText($review)
{
    return $review['reviewText'] != "";
}

function filterWithoutText($review)
{
    return $review['reviewText'] == "";
}

function old($name, $value)
{
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        return $_POST[$name] == $value ? "selected" : "";
    }
    return;
}