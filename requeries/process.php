<?php


// FILTER API

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($_POST['prioritizeByText']) {

        $reviews = array_filter($reviews, "filterByMinimumRating");
        $reviewsWithText = array_filter($reviews, "filterWithText");
        $reviewsWithoutText = array_filter($reviews, "filterWithoutText");
        usort($reviewsWithText, "sortByRatingAndDate");
        usort($reviewsWithoutText, "sortByRatingAndDate");
        $reviews = array_merge($reviewsWithText, $reviewsWithoutText);

    } else {

        $reviews = array_filter($reviews, "filterByMinimumRating");
        usort($reviews, "sortByRatingAndDate");
    }

}