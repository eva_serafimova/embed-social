<?php

require_once 'requeries.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-6">
                <!----------------------------- FILTERING FORM ------------------------------->
                <h5>Filter reviews</h5>
                <form method="POST">
                    <div class="form-group">
                        <label for="rating">Order by rating:</label>
                        <select class="form-control" name="sortByRating" id="sortByRating">
                            <option <?= old("sortByRating", "highest") ?> value="highest">Highest First</option>
                            <option <?= old("sortByRating", "lowest") ?> value="lowest">Lowest First</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="filterByRating">Minimum rating:</label>
                        <select class="form-control" name="filterByRating" id="filterByRating">
                            <?php
                            for ($i = 5; $i >= 1; $i--) { ?>
                                <option <?= old("filterByRating", $i) ?> value=<?= $i ?>> <?= $i ?> </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sortByDate">Order by date:</label>
                        <select class="form-control" name="sortByDate" id="sortByDate">
                            <option <?= old("sortByDate", "oldest") ?> value="oldest">Oldest First</option>
                            <option <?= old("sortByDate", "newest") ?> value="newest">Newest First</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="prioritizeByText">Prioritize by text:</label>
                        <select class="form-control" name="prioritizeByText" id="prioritizeByText">
                            <option <?= old("prioritizeByText", 1) ?> value="1">Yes</option>
                            <option <?= old("prioritizeByText", 0) ?> value="0">No</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Filter</button>
                </form>
            </div>
            <div class="col-12 my-5">
                <!----------------------------- REVIEWS TABLE ------------------------------->
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Rating</th>
                            <th scope="col">Review date</th>
                            <th scope="col">Review time</th>
                            <th scope="col">Review text</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($reviews as $key => $review) {
                            $iterationNumber = $key + 1;
                            $dateAndTime = explode('T', $review['reviewCreatedOnDate']);
                            $date = $dateAndTime[0];
                            $time = explode('+', $dateAndTime[1]);
                            $time = $time[0];
                            $rating = $review['rating'];
                            $text = $review['reviewText'];
                        ?>
                            <tr>
                                <th scope="row"><?= $iterationNumber ?></th>
                                <td><?= $rating ?></td>
                                <td><?= $date ?></td>
                                <td><?= $time ?></td>
                                <td><?= $text ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>